/** @type {import('tailwindcss').Config} */
export default {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		extend: {
			padding: {
				495: '0 0 calc(100% * 495/2048) 0'
			}
		}
	},
	plugins: []
}
